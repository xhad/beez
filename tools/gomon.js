#!/usr/bin/env node
const fs = require('fs')
const path = require('path')
const spawn = require('child_process').spawn
const exec = require('child_process').exec
const args = require('minimist')(process.argv.slice(2))._
const watch = require('watch')
const colorsTmpl = require('colors')

const BINARY_FILE = 'build/beezd'

let gorun;

if (args.length <= 0 || args > 1) {
  console.error([
    'Use gomon like so:\n',
    'gomon myfile.go\n'.green,
    'or even:\n',
    'gomon *.go\n'.green
  ].join(''))
  process.exit(2)
}

const goFile = args[0]
const procArgs = process.argv.slice(3)

const firstArgDir = path.dirname(goFile)
const mainDir = firstArgDir === '.' ? process.cwd() : firstArgDir

watch.createMonitor(mainDir, { ignoreDotFiles: true }, function (monitor) {
  monitor.on('created', conditionalKill('created'))
  monitor.on('changed', conditionalKill('changed'))
  monitor.on('removed', conditionalKill('removed'))
})

function run () {
  console.log('>> Bitcoin Center Korea Dev Shop'.bgBlue)
    const buildCommand = 'go build -o ' + BINARY_FILE

  exec(buildCommand, function (err, stdout, stderr) {
    if (err) {
      console.error(err)
      return
    }
    if (stdout) {
      console.log(stdout)
      return
    }
    if (stderr) {
      console.error(stderr)
      return
    }

    const runCommand = './' + BINARY_FILE
    gorun = spawn(runCommand, procArgs, { stdio: 'inherit' })
  })
}

function conditionalKill (reason) {
  return function (f) {
    if (!f.endsWith(BINARY_FILE)) {
      const message = '\n>> restarting due to ' + reason + ' file: ' + f
      console.log(message.cyan)
      killAll()
    }
  }
}

function killAll () {
  if (!gorun) { run() } else {
    exec('kill ' + gorun.pid, function (err, stdout, stderr) {
      run()
    })
  }
}

run()
